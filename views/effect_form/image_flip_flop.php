<?php
defined( 'ABSPATH' ) || exit;

/* @var $effect array */
/* @var $preset array */
/* @var $action string */
/* @var $effect_key string */
?>
<h2><?php _e( 'Image flip & flop (reflect) settings', WPDI_Common::PLUGIN_SYSTEM_NAME ) ?></h2>
<p><?php _e( 'Reflect image with selected direction.', WPDI_Common::PLUGIN_SYSTEM_NAME ) ?></p>
<form method="post"
      action="<?php echo WPDI_Common::build_plugin_url( array(
	      'action'     => @$action,
	      'thumb'      => @$preset['wpdi_name'],
	      'effect'     => @$effect['wpdi_effect'],
	      'effect_key' => (string) @$effect_key
      ) ) ?>">
    <table class="form-table" role="presentation">
        <tbody>
        <tr class="field-effect-direction">
            <th><?php _e( 'Direction', WPDI_Common::PLUGIN_SYSTEM_NAME ) ?></th>
            <td>
                <div class="form-item form-type-radio form-item-data-direction">
                    <label for="field-effect-direction-flip">
                        <input title="<?php _e( 'Flip(reflect vertical)', WPDI_Common::PLUGIN_SYSTEM_NAME ) ?>"
                               type="radio" id="field-effect-direction-flip" name="wpdi_effect_direction"
                               value="flip" class="form-radio"
							<?php echo checked( 'flip', @$effect['wpdi_effect_direction'] ) ?>>
						<?php _e( 'Flip(reflect vertical)', WPDI_Common::PLUGIN_SYSTEM_NAME ) ?>
                    </label>
                </div>

                <div class="form-item form-type-radio form-item-data-direction">
                    <label for="field-effect-direction-flop">
                        <input title="<?php _e( 'Flop(reflect horizontal)', WPDI_Common::PLUGIN_SYSTEM_NAME ) ?>"
                               type="radio" id="field-effect-direction-flop" name="wpdi_effect_direction"
                               value="flop" class="form-radio"
							<?php echo checked( 'flop', @$effect['wpdi_effect_direction'] ) ?>>
						<?php _e( 'Flop(reflect horizontal)', WPDI_Common::PLUGIN_SYSTEM_NAME ) ?>
                    </label>
                </div>
            </td>
        </tr>
        </tbody>
    </table>
    <p class="submit">
        <input type="hidden" name="wpdi_effect" value="<?php echo $effect['wpdi_effect'] ?>">
        <input type="submit" id="submit" class="button-primary"
               value="<?php _e( 'Save Effect', WPDI_Common::PLUGIN_SYSTEM_NAME ) ?>"/>
        <a class="button" onclick="window.history.back();">
			<?php _e( 'back', WPDI_Common::PLUGIN_SYSTEM_NAME ) ?>
        </a>
    </p>
</form>