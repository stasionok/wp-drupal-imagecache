<?php
defined( 'ABSPATH' ) || exit;

/* @var $effect array */
/* @var $preset array */
/* @var $action string */
/* @var $effect_key string */

$checked = ! empty( $effect['wpdi_effect_independent_corners']['tl'] ) OR ! empty( $effect['wpdi_effect_independent_corners']['tr'] ) OR ! empty( $effect['wpdi_effect_independent_corners']['bl'] ) OR ! empty( $effect['wpdi_effect_independent_corners']['br'] );
$is_imagick = extension_loaded( 'imagick' );
?>
<h2><?php _e( 'Image round corners settings', WPDI_Common::PLUGIN_SYSTEM_NAME ) ?></h2>
<p><?php _e( 'This is true cropping, not overlays, so the result can be transparent.', WPDI_Common::PLUGIN_SYSTEM_NAME ) ?></p>
<form method="post"
      action="<?php echo WPDI_Common::build_plugin_url( array(
	      'action'     => @$action,
	      'thumb'      => @$preset['wpdi_name'],
	      'effect'     => @$effect['wpdi_effect'],
	      'effect_key' => (string) @$effect_key
      ) ) ?>">
    <table class="form-table" role="presentation">
        <tbody>
        <tr class="field-effect-radius">
            <th><label for="field-effect-radius"><?php _e( 'Radius', WPDI_Common::PLUGIN_SYSTEM_NAME ) ?></label>
            </th>
            <td>
                <input type="text" name="wpdi_effect_radius" id="field-effect-radius" class="regular-text"
                       value="<?php esc_attr_e( @$effect['wpdi_effect_radius'] ); ?>">
                <span class="description"><?php _e( 'in pixels', WPDI_Common::PLUGIN_SYSTEM_NAME ) ?></span>
            </td>
        </tr>
		<?php if ( ! $is_imagick ): ?>
            <tr class="field-effect-radius">
                <td></td>
                <td>
                    <label for="independent_corners">
                        <input type="checkbox" id="independent_corners" <?php if ( $checked ): ?>checked<?php endif; ?>>
						<?php _e( 'Individual corner values', WPDI_Common::PLUGIN_SYSTEM_NAME ) ?>
                    </label>
                    <span class="description"><?php _e( 'Set Corners Independently', WPDI_Common::PLUGIN_SYSTEM_NAME ) ?></span>
                </td>
            </tr>
            <tr class="hidden-field-indep-corn field-effect-tl">
                <th>
                    <label for="field-effect-tl"><?php _e( 'Top Left Radius', WPDI_Common::PLUGIN_SYSTEM_NAME ) ?></label>
                </th>
                <td>
                    <input type="text" name="wpdi_effect_independent_corners_tl" id="field-effect-tl"
                           class="regular-text"
                           value="<?php esc_attr_e( @$effect['wpdi_effect_independent_corners_tl'] ); ?>">
                    <span class="description"><?php _e( 'in pixels', WPDI_Common::PLUGIN_SYSTEM_NAME ) ?></span>
                </td>
            </tr>
            <tr class="hidden-field-indep-corn field-effect-tr">
                <th>
                    <label for="field-effect-tr"><?php _e( 'Top Right Radius', WPDI_Common::PLUGIN_SYSTEM_NAME ) ?></label>
                </th>
                <td>
                    <input type="text" name="wpdi_effect_independent_corners_tr" id="field-effect-tr"
                           class="regular-text"
                           value="<?php esc_attr_e( @$effect['wpdi_effect_independent_corners_tr'] ); ?>">
                    <span class="description"><?php _e( 'in pixels', WPDI_Common::PLUGIN_SYSTEM_NAME ) ?></span>
                </td>
            </tr>
            <tr class="hidden-field-indep-corn field-effect-bl">
                <th>
                    <label for="field-effect-bl"><?php _e( 'Bottom Left Radius', WPDI_Common::PLUGIN_SYSTEM_NAME ) ?></label>
                </th>
                <td>
                    <input type="text" name="wpdi_effect_independent_corners_bl" id="field-effect-bl"
                           class="regular-text"
                           value="<?php esc_attr_e( @$effect['wpdi_effect_independent_corners_bl'] ); ?>">
                    <span class="description"><?php _e( 'in pixels', WPDI_Common::PLUGIN_SYSTEM_NAME ) ?></span>
                </td>
            </tr>
            <tr class="hidden-field-indep-corn field-effect-br">
                <th>
                    <label for="field-effect-br"><?php _e( 'Bottom Right Radius', WPDI_Common::PLUGIN_SYSTEM_NAME ) ?></label>
                </th>
                <td>
                    <input type="text" name="wpdi_effect_independent_corners_br" id="field-effect-br"
                           class="regular-text"
                           value="<?php esc_attr_e( @$effect['wpdi_effect_independent_corners_br'] ); ?>">
                    <span class="description"><?php _e( 'in pixels', WPDI_Common::PLUGIN_SYSTEM_NAME ) ?></span>
                </td>
            </tr>
		<?php endif; ?>
        </tbody>
    </table>
    <p class="submit">
        <input type="hidden" name="wpdi_effect" value="<?php echo $effect['wpdi_effect'] ?>">
        <input type="submit" id="submit" class="button-primary"
               value="<?php _e( 'Save Effect', WPDI_Common::PLUGIN_SYSTEM_NAME ) ?>"/>
        <a class="button" onclick="window.history.back();">
			<?php _e( 'back', WPDI_Common::PLUGIN_SYSTEM_NAME ) ?>
        </a>
    </p>
</form>
<?php if ( ! $checked ): ?>
    <style>
        .hidden-field-indep-corn {
            display: none;
        }
    </style>
<?php endif; ?>
<script>
    jQuery('#independent_corners').on('click', function () {
        jQuery('.hidden-field-indep-corn').toggle();
    });
</script>
